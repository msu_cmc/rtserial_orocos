extern "C" {
#include  <byteswap.h>
}

#include <rtt/RTT.hpp>
#include <rtt/plugin/Plugin.hpp>
#include <rtt/plugin/ServicePlugin.hpp>

#include <rtt/marsh/Marshalling.hpp>

#include "CoderInt16-plugin.hpp"

using namespace RTT;
using namespace std;

CoderInt16::CoderInt16(RTT::TaskContext* context) :
	CoderService("CoderInt16", context),
	array_size(0)
{
	this->doc("Frame is encoded as the array of little-endian int16.");
	// Add properies.
	getOwner()->provides()->addProperty("byteswap", byteswap_prop).
		doc("Reverse byte order numbers.").set(false);
	// Add port.
	getOwner()->addEventPort("in", in_port).
		doc("Input data port.");
	// Add properies.
	getOwner()->provides()->addProperty("array_size", array_size_prop).
		doc("Number of int16 stored in frame.").set(0);
	// Attempt load property from config file.
	//getOwner()->getProvider<Marshalling>("marshalling")->readProperty( this->getName() + ".array_size", context->getName() + ".cpf");

	log(Info) << "CoderInt16 plugin is loaded !" << endlog();	
}

CoderInt16::~CoderInt16()
{
	getOwner()->provides()->removeProperty(byteswap_prop);
	getOwner()->provides()->removeProperty(array_size_prop);
	getOwner()->provides()->removePort("in");
}

bool CoderInt16::configure() 
{
	int size = array_size_prop.get();
	if (size <= 0 || size >= this->max_array_size) {
		log(Error) << "CoderInt16 plugin: incorrect 'array_size' property value: " << size << endlog(); 
		return false;
	}
	array_size = size;
	// set buffer size so resize is not called on realtime.
	port_buffer.resize(array_size);
	byteswap = byteswap_prop.get();
	log(Info) << "CoderInt16 plugin is reconfigured." << endlog();	
	return true;
}

bool CoderInt16::encode(char * buffer) 
{
	if (NewData != in_port.read(port_buffer, false)) {
		// no new data available
		return false;
	}
	if (port_buffer.size() != array_size) {
		//error: sample size changed while component is running.
		log(Error) << "CoderInt16 plugin: incorect input sample received" << endlog();
		this->getOwner()->error();
	}
	//copy to array
	int16_t tmp;
	for(int i = 0; i < array_size; i++) { 
		tmp = port_buffer[i];
		if (byteswap) *(reinterpret_cast<int16_t *>(buffer)) = bswap_16(tmp);
		else *(reinterpret_cast<int16_t *>(buffer)) = tmp;
		buffer += 2;
	}
	return true;
}

ORO_SERVICE_NAMED_PLUGIN( CoderInt16, "CoderInt16")
