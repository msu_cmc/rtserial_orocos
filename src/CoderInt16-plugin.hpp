#ifndef  CODERINT16_PLUGIN_HPP
#define  CODERINT16_PLUGIN_HPP

#include <rtt/Port.hpp>
#include <rtt/Service.hpp>

#include "RTSerial-types.hpp"
#include "CoderService.hpp"

/**
 * @ingroup Coder
 * @brief Coder service plugin for array of int16.
 * Receive on input port array of integer and copy them
 * to frame buffer as array of litle-endian 16-bit ints
 * Size of the array is may be chanded if component is stoped.
 **/
class CoderInt16 : public CoderService
{
	RTT::InputPort< std::vector<double> > in_port; ///< Input port.
	RTT::Property<int> array_size_prop;  ///< Array size property.
	RTT::Property<bool> byteswap_prop; ///< Reverse byte order in received ints.
	std::vector<double> port_buffer; ///< Temporary buffer for input date.

	int array_size; ///< Current real array size. May be changed only by @ref configure call, when the conponent is not running.
	bool byteswap; ///< Cached byteswap_prop value.
public:
	static int const max_array_size = 20; ///< maximal array size.
public:
	CoderInt16(RTT::TaskContext* c);
	~CoderInt16();
	
	bool configure();
	int getDataSize() const {
		return 2*array_size;
	}
	bool encode(char * buffer);
};

#endif  /*CODERINT16-PLUGIN_HPP*/
