extern "C" {
#include  <byteswap.h>
}

#include <iostream>

#include <rtt/RTT.hpp>
#include <rtt/plugin/Plugin.hpp>
#include <rtt/plugin/ServicePlugin.hpp>

#include "DataConversation.hpp"
#include "CoderManipPPPD-plugin.hpp"

using namespace RTT;
using namespace std;
using namespace DataConversation;

CoderManipPPPD::CoderManipPPPD(RTT::TaskContext* context) :
	CoderService("CoderManipPPPD", context),
	njoints(0)
{
	this->doc("Manipulator control plugin.");
	// Add port.
	getOwner()->addEventPort("ref", ref_port).
		doc("Reference position.");
	getOwner()->addPort("pwm", pwm_port).
		doc("Additional PWM correction (range from -1 to 1).");
	getOwner()->addPort("kp", kp_port).
		doc("Kp coefficient of PD-regulator (signed 7.8 fixed point).");
	getOwner()->addPort("kd", kd_port).
		doc("Kd coefficient of PD-regulator (signed 7.8 fixed point).");
	// Add properies.
	getOwner()->provides()->addProperty("njoints", njoints_prop).
		doc("Number of manipulator joints.").set(0);
	getOwner()->provides()->addProperty("ref_scale", ref_scale_prop).
		doc("Position scale factor.");
	getOwner()->provides()->addProperty("ref_offset", ref_offset_prop).
		doc("Position offset.");
	// Attempt load property from config file.
	//getOwner()->getProvider<Marshalling>("marshalling")->readProperty( this->getName() + ".array_size", context->getName() + ".cpf");

	log(Info) << "CoderManipPPPD plugin is loaded !" << endlog();	
}

CoderManipPPPD::~CoderManipPPPD()
{
	getOwner()->provides()->removeProperty(njoints_prop);
	getOwner()->provides()->removeProperty(ref_scale_prop);
	getOwner()->provides()->removeProperty(ref_offset_prop);
	getOwner()->provides()->removePort("ref");
	getOwner()->provides()->removePort("pwm");
	getOwner()->provides()->removePort("kp");
	getOwner()->provides()->removePort("kd");
}

bool CoderManipPPPD::configure() 
{
	int size = njoints_prop.get();
	if (size <= 0 || size >= 10) {
		log(Error) << "CoderManipPPPD plugin: incorrect 'njoints' property value: " << size << endlog(); 
		return false;
	}
	njoints = size;
	// set buffer size so resize is not called on realtime.
	ref.resize(njoints, 0);
	pwm.resize(njoints, 0);
	kp.resize(njoints, 0);
	kd.resize(njoints, 0);
	//
	// Check conversation parameters
	CheckConvParameter(ref_scale_prop.value(), njoints, 1.0, "CoderManipPPPD: ref_scale_porp size incorrect.");
	CheckConvParameter(ref_offset_prop.value(), njoints, 0.0, "CoderManipPPPD: ref_offset_porp size incorrect.");

	log(Info) << "CoderManipPPPD plugin is reconfigured." << endlog();	
	return true;
}

bool CoderManipPPPD::encode(char * buffer) 
{
	ref_port.read(ref);
	pwm_port.read(pwm);
	kp_port.read(kp);
	kd_port.read(kd);

	/*if (port_buffer.size() != array_size) {
		//error: sample size changed while component is running.
		log(Error) << "CoderInt16 plugin: incorect input sample received" << endlog();
		this->getOwner()->error();
	}*/
	for(int i = 0; i < njoints; i++) { 
		*(reinterpret_cast<int16_t *>(buffer)) = bswap_16( to_unsigned_int<16>( InvScaleOffset( ref[i], ref_scale_prop.get()[i], ref_offset_prop.get()[i] ) ) );
		*(reinterpret_cast<int16_t *>(buffer+2)) = bswap_16( to_unsigned_int<16>( pwm[i] * 0x03ff) ); // map (-1, 1) on 11-bit signed integer
		// TODO normalization
		*(reinterpret_cast<int16_t *>(buffer+4)) = bswap_16( (to_signed_fixed_point<7,8>(kp[i])) );
		*(reinterpret_cast<int16_t *>(buffer+6)) = bswap_16( (to_signed_fixed_point<7,8>(kd[i])) );
		buffer += 8;
	}
	return true;
}

ORO_SERVICE_NAMED_PLUGIN( CoderManipPPPD, "CoderManipPPPD")
