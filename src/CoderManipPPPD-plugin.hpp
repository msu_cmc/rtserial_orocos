#ifndef  CODERMANIPPPPD_PLUGIN_HPP
#define  CODERMANIPPPPD_PLUGIN_HPP

#include <rtt/Port.hpp>
#include <rtt/Service.hpp>

#include "RTSerial-types.hpp"
#include "CoderService.hpp"

/**
 * @ingroup Coder
 * @brief Coder service plugin for manipulator control.
 * Receive on input ports arrays of referense position, 
 * PWM correction, Kp and Kd coefficients of PD regulators.
 * Code them in array of big-endian 16-bit intergers. 
 * Number of joints can be changed.
 **/
class CoderManipPPPD : public CoderService
{
	RTT::InputPort< std::vector<double> > ref_port; ///< Reference position.
	RTT::InputPort< std::vector<double> > pwm_port; ///< PWM correction
	RTT::InputPort< std::vector<double> > kp_port; ///< Proportional coefficient of PD-regulator.
	RTT::InputPort< std::vector<double> > kd_port; ///< Differential coefficient of PD-regulator.

	RTT::Property<int> njoints_prop;  ///< Number of joints.

	RTT::Property< std::vector<double> > ref_scale_prop;  ///< Position scale factor.
	RTT::Property< std::vector<double> > ref_offset_prop;  ///< Position offset.

	std::vector<double> ref, pwm, kp, kd; ///< Temporary buffers for input date.
	int njoints; ///< Number of joints.

public:
	CoderManipPPPD(RTT::TaskContext* c);
	~CoderManipPPPD();
	
	bool configure();
	int getDataSize() const {
		return 2*4*njoints;
	}
	bool encode(char * buffer);
};

#endif  /*CODERMANIPPPPD_HPP*/
