extern "C" {
#include  <byteswap.h>
}

#include <iostream>

#include <rtt/RTT.hpp>
#include <rtt/plugin/Plugin.hpp>
#include <rtt/plugin/ServicePlugin.hpp>

#include "DataConversation.hpp"
#include "CoderManipPSPPD-plugin.hpp"

using namespace RTT;
using namespace std;
using namespace DataConversation;

CoderManipPSPPD::CoderManipPSPPD(RTT::TaskContext* context) :
	CoderService("CoderManipPSPPD", context),
	njoints(0)
{
	this->doc("Manipulator control plugin.");
	// Add port.
	getOwner()->addEventPort("ref_position", ref_position_port).
		doc("Reference position.");
	getOwner()->addEventPort("ref_speed", ref_speed_port).
		doc("Reference speed.");
	getOwner()->addPort("pwm", pwm_port).
		doc("Additional PWM correction (in range from -1 to 1).");
	getOwner()->addPort("kp", kp_port).
		doc("Kp coefficient of PD-regulator (signed 7.8 fixed point).");
	getOwner()->addPort("kd", kd_port).
		doc("Kd coefficient of PD-regulator (signed 7.8 fixed point).");
	// Add properies.
	getOwner()->provides()->addProperty("njoints", njoints_prop).
		doc("Number of manipulator joints.").set(0);
	getOwner()->provides()->addProperty("position_scale", position_scale_prop).
		doc("Position scale factor.");
	getOwner()->provides()->addProperty("position_offset", position_offset_prop).
		doc("Position offset.");
	getOwner()->provides()->addProperty("speed_scale", speed_scale_prop).
		doc("Position scale factor.");
	// Attempt load property from config file.
	//getOwner()->getProvider<Marshalling>("marshalling")->readProperty( this->getName() + ".array_size", context->getName() + ".cpf");

	log(Info) << "CoderManipPSPPD plugin is loaded !" << endlog();	
}

CoderManipPSPPD::~CoderManipPSPPD()
{
	getOwner()->provides()->removeProperty(njoints_prop);
	getOwner()->provides()->removeProperty(position_scale_prop);
	getOwner()->provides()->removeProperty(position_offset_prop);
	getOwner()->provides()->removeProperty(speed_scale_prop);
	getOwner()->provides()->removePort("ref_position");
	getOwner()->provides()->removePort("ref_speed");
	getOwner()->provides()->removePort("pwm");
	getOwner()->provides()->removePort("kp");
	getOwner()->provides()->removePort("kd");
}

bool CoderManipPSPPD::configure() 
{
	int size = njoints_prop.get();
	if (size <= 0 || size >= 10) {
		log(Error) << "CoderManipPSPPD plugin: incorrect 'njoints' property value: " << size << endlog(); 
		return false;
	}
	njoints = size;
	// set buffer size so resize is not called on realtime.
	ref_position.resize(njoints, 0);
	ref_speed.resize(njoints, 0);
	pwm.resize(njoints, 0);
	kp.resize(njoints, 0);
	kd.resize(njoints, 0);
	// Check conversation parameters
	CheckConvParameter(position_scale_prop.value(), njoints, 1.0, "CoderManipPSPPD: position_scale_porp size incorrect.");
	CheckConvParameter(position_offset_prop.value(), njoints, 0.0, "CoderManipPSPPD: position_offset_porp size incorrect.");
	CheckConvParameter(speed_scale_prop.value(), njoints, 1.0, "CoderManipPSPPD: speed_scale_porp size incorrect.");

	log(Info) << "CoderManipPSPPD plugin is reconfigured." << endlog();	
	return true;
}


bool CoderManipPSPPD::encode(char * buffer) 
{
	union pos_speed_pwm_packed_t {
		volatile uint32_t raw;
		struct {
			volatile unsigned position : 10;
			volatile signed speed : 11;
			volatile signed pwm : 11;
		} fields;
	};

	ref_position_port.read(ref_position);
	ref_speed_port.read(ref_speed);
	pwm_port.read(pwm);
	kp_port.read(kp);
	kd_port.read(kd);

	/*if (port_buffer.size() != array_size) {
		//error: sample size changed while component is running.
		log(Error) << "CoderInt16 plugin: incorect input sample received" << endlog();
		this->getOwner()->error();
	}*/
	//copy to array
	pos_speed_pwm_packed_t tmp;

	for(int i = 0; i < njoints; i++) {
		tmp.fields.position = to_unsigned_int<10>( InvScaleOffset( ref_position[i], position_scale_prop.get()[i], position_offset_prop.get()[i] ) );
		tmp.fields.speed = to_signed_int<11>( InvScale( ref_speed[i], speed_scale_prop.get()[i] ) );
		tmp.fields.pwm = to_signed_int<11>( pwm[i] * 0x03ff ); // map (-1,1) range to 11-bit signed integer
		*(reinterpret_cast<int32_t *>(buffer)) = tmp.raw;
		*(reinterpret_cast<int16_t *>(buffer+4)) = bswap_16( (to_signed_fixed_point<7,8>(kp[i])) );
		*(reinterpret_cast<int16_t *>(buffer+6)) = bswap_16( (to_signed_fixed_point<7,8>(kd[i])) );
		buffer += 8;
	}
	return true;
}

ORO_SERVICE_NAMED_PLUGIN( CoderManipPSPPD, "CoderManipPSPPD")
