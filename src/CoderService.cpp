#include "rtt/TaskContext.hpp"

#include "CoderService.hpp"

CoderService::CoderService(const std::string& name, RTT::TaskContext * context) :
	RTT::Service(name, context) 
{
	// Add operations.
	getOwner()->addOperation("coderConfigure", &CoderService::configure, this).
		doc("Input port reconfiguration. Change port width accoding to array_size.");
	getOwner()->addOperation("coderEncode", &CoderService::encode, this).
		doc("Read input port and write data to buffer");
	getOwner()->addOperation("coderGetDataSize", &CoderService::getDataSize, this).
		doc("Return data block length in bytes.");
}

CoderService::~CoderService() 
{
	getOwner()->provides()->removeOperation("coderEncode");
	getOwner()->provides()->removeOperation("coderGetDataSize");
	getOwner()->provides()->removeOperation("coderConfigure");
}
