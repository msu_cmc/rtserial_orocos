#ifndef  CODERINTERFACE_HPP
#define  CODERINTERFACE_HPP

#include <rtt/Service.hpp>

/**
 * @defgroup Coder
 * @brief Coder services.
 * @{
 */

/**
 * @brief Coder service interface.
 **/
class CoderInterface
{
	public:
		/**
		 * @brief Reconfigure coder.
		 * Configure coder and input ports accoding to properies.
		 * @return true on success.
		 **/
		virtual bool configure() = 0;

		/**
		 * @brief Get requied data buffer size.
		 **/
		virtual int getDataSize() const = 0; 

		/**
		 * @brief Read input ports, encode date and copy it to buffer.
		 * If new data is not available, @a buffer content is not changed.
		 * @param pointer to the begin of data block in frame buffer.
		 * @return true if new data was received, false otherwise.
		 */
		virtual bool encode(char * buffer) = 0;

		/**
		 * @brief Desructor.
		 * Desructor.
		 **/
		virtual ~CoderInterface() {}
};

/**
 * @brief Coder service base calss.
 **/
class CoderService : 
	public CoderInterface, 
	public RTT::Service 
{
	public:
		CoderService(const std::string& name, RTT::TaskContext * context);
		virtual ~CoderService();
};
/**
 * @}
 */
#endif  /*CODERINTERFACE_HPP*/
