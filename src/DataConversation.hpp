#ifndef  DATACONVERSATIONS_HPP
#define  DATACONVERSATIONS_HPP

#include <vector>
#include <string>

#include <rtt/Logger.hpp>

/**
 * @brief Data conversation helper functions.
 * Data conversation functions are used by @ref Coder and @ref Decode calsses.
 *
 **/
namespace DataConversation {
	template <int bitlength, typename T>
	inline int to_signed_int(T val) 
	{
		int max = (1 << (bitlength - 1)) - 1;
		int min = -(1 << (bitlength - 1));
		if ( val >= static_cast<T>(min) ) {
			if ( val <= static_cast<T>(max) ) return static_cast<int>(val);
			else return max;
		}
		else return min;
	}

	template <int bitlength, typename T>
	inline unsigned int to_unsigned_int(T val) 
	{
		unsigned int max = (1 << bitlength) - 1;
		if ( val >= 0 ) {
			if ( val <= static_cast<T>(max) ) return static_cast<unsigned int>(val);
			else return max;
		}
		else return 0;
	}

	template <int int_bits, int fraction_bits, typename T>
	inline int to_signed_fixed_point(T val) {
		return to_signed_int<int_bits + fraction_bits + 1>(val * (1 << fraction_bits));
	}

	template <typename T>
	inline T Scale(T data, T scale)
	{
		 return scale * data;
	}

	template <typename T>
	inline T InvScale(T data, T scale)
	{
		return data/scale;
	}

	template <typename T>
	inline T ScaleOffset(T data, T scale, T offset)
	{
		 return scale * (data - offset);
	}

	template <typename T>
	inline T InvScaleOffset(T data, T scale, T offset)
	{
		return data/scale + offset;
	}

	template <typename T>
	bool CheckConvParameter(std::vector<T>& param, int size, T def_value, const std::string& err_msg)
	{
		if (param.size() != size) {
			if (param.size() != 0) RTT::log(RTT::Warning) << err_msg << RTT::endlog();
			param.assign(size, def_value);
		}
		return true;
	}
};

#endif  /*DATACONVERSATIONS_HPP*/
