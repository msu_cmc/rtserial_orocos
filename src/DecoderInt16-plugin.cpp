extern "C" {
#include  <byteswap.h>
}

#include <rtt/RTT.hpp>
#include <rtt/plugin/Plugin.hpp>
#include <rtt/plugin/ServicePlugin.hpp>

#include <rtt/marsh/Marshalling.hpp>

#include "DecoderInt16-plugin.hpp"

using namespace RTT;
using namespace std;

DecoderInt16::DecoderInt16(RTT::TaskContext* context) :
	DecoderService("DecoderInt16", context),
	array_size(0)
{
	this->doc("Frame is decoded as array of little-endian int16.");
	// ports
	getOwner()->addPort("out", out_port).
		doc("Output data port.");
	// properies
	getOwner()->provides()->addProperty("array_size", array_size_prop).
		doc("Number of int16 stored in frame.").set(0);
	getOwner()->provides()->addProperty("byteswap", byteswap_prop).
		doc("Reverse byte order in int16 numbers.").set(false);
	// Attempt load property from config file.
	// getOwner()->getProvider<Marshalling>("marshalling")->readProperty( this->getName() + ".array_size", context->getName() + ".cpf");
	log(Info) << "DecoderInt16 plugin is loaded !" << endlog();	
}

DecoderInt16::~DecoderInt16()
{
	getOwner()->provides()->removeProperty(array_size_prop);
	getOwner()->provides()->removeProperty(byteswap_prop);
	getOwner()->provides()->removePort("out");
	getOwner()->provides()->removeOperation("decoderDecode");
	getOwner()->provides()->removeOperation("decoderGetDataSize");
	getOwner()->provides()->removeOperation("decoderConfigure");
}

bool DecoderInt16::configure() 
{
	int size = array_size_prop.get();
	if (size <= 0 || size >= this->max_array_size) {
		log(Error) << "DecoderInt16 plugin: incorrect 'array_size' property value: " << size << endlog(); 
		return false;
	}
	array_size = size;
	// set port size
	decoded_buffer.resize(array_size);
	out_port.setDataSample(decoded_buffer);
	byteswap = byteswap_prop.get();
	log(Info) << "DecoderInt16 plugin is reconfigured." << endlog();	
	return true;
}

void DecoderInt16::decode(const char * buffer) 
{
	for(int i = 0; i < array_size; i++) { 
		if (byteswap) decoded_buffer[i] = bswap_16(*(reinterpret_cast<const int16_t *>(buffer)));
		else decoded_buffer[i] = *(reinterpret_cast<const int16_t *>(buffer)); 
		buffer += 2;
	}
	out_port.write(decoded_buffer);
}

ORO_SERVICE_NAMED_PLUGIN( DecoderInt16, "DecoderInt16")
