#ifndef  DECODER16_PLUGIN_HPP
#define  DECODER16_PLUGIN_HPP

#include <rtt/Port.hpp>
#include <rtt/Service.hpp>

#include "RTSerial-types.hpp"
#include "DecoderService.hpp"

/**
 * @ingroup Decoder
 * @brief Decode service plugin for array of int16.
 * Decode input buffer as the array of little-endian 16-bit integers.
 * Size of the array is fixed. May reverce byte order in received ints.
 **/
class DecoderInt16 : public DecoderService
{
	RTT::OutputPort< std::vector<double> > out_port; ///< Output port
	RTT::Property<int> array_size_prop;  ///< Array size property.
	RTT::Property<bool> byteswap_prop; ///< Reverse byte order in received ints.
	
	std::vector<double> decoded_buffer; ///< Temporary buffer for decoded data.
	int array_size; ///< Current real array size. May be changed only by @ref configure call, when the conponent is sopped.
	bool byteswap; ///< Cached byteswap_prop value.
public:
	static int const max_array_size = 20; ///< maximal array size.
public:
	DecoderInt16(RTT::TaskContext* c);
	~DecoderInt16();
	
	bool configure();
	int getDataSize() const {
		return 2*array_size;
	}
	void decode(const char * buffer);
};

#endif  /*DECODER16-PLUGIN_HPP*/
