extern "C" {
#include  <byteswap.h>
}

#include <rtt/RTT.hpp>
#include <rtt/plugin/Plugin.hpp>
#include <rtt/plugin/ServicePlugin.hpp>

#include <rtt/marsh/Marshalling.hpp>

#include "DataConversation.hpp"
#include "DecoderManipPSCP-plugin.hpp"

using namespace RTT;
using namespace std;
using namespace DataConversation;

DecoderManipPSCP::DecoderManipPSCP(RTT::TaskContext* context) :
	DecoderService("DecoderManipPSCP", context),
	njoints(0)
{
	this->doc("Manipulator input plugin.");
	// ports
	getOwner()->addPort("position", pos_port).
		doc("Position (10-bits).");
	getOwner()->addPort("speed", speed_port).
		doc("Speed.");
	getOwner()->addPort("current", current_port).
		doc("Current (10-bits).");
	getOwner()->addPort("pwm", pwm_port).
		doc("PWM value in range from (-1, 1).");
	// properies
	getOwner()->provides()->addProperty("njoints", njoints_prop).
		doc("Number of manipulator joints.");
	// data conversation
	getOwner()->provides()->addProperty("pos_scale", pos_scale_prop).
		doc("Position scale factor.");
	getOwner()->provides()->addProperty("pos_offset", pos_offset_prop).
		doc("Position offset.");
	getOwner()->provides()->addProperty("speed_scale", speed_scale_prop).
		doc("Speed scale factor.");
	getOwner()->provides()->addProperty("current_scale", current_scale_prop).
		doc("Current scale factor.");
	// Attempt load property from config file.
	// getOwner()->getProvider<Marshalling>("marshalling")->readProperty( this->getName() + ".array_size", context->getName() + ".cpf");
	log(Info) << "DecoderManipPSCP plugin is loaded !" << endlog();	
}

DecoderManipPSCP::~DecoderManipPSCP()
{
	getOwner()->provides()->removeProperty(njoints_prop);
	getOwner()->provides()->removePort("position");
	getOwner()->provides()->removePort("speed");
	getOwner()->provides()->removePort("current");
	getOwner()->provides()->removePort("pwm");
}

bool DecoderManipPSCP::configure() 
{
	int size = njoints_prop.get();
	if (size <= 0 || size >= 10) {
		log(Error) << "DecoderManipPSCP plugin: incorrect 'njoints' property value: " << size << endlog(); 
		return false;
	}
	njoints = size;
	// set port size
	pos.resize(njoints);
	speed.resize(njoints);
	current.resize(njoints);
	pwm.resize(njoints);

	pos_port.setDataSample(pos);
	speed_port.setDataSample(speed);
	current_port.setDataSample(current);
	pwm_port.setDataSample(pwm);

	// Check conversation parameters
	CheckConvParameter(pos_scale_prop.value(), njoints, 1.0, "DecoderManipPSCP: pos_scale_prop size incorrect.");
	CheckConvParameter(pos_offset_prop.value(), njoints, 0.0, "DecoderManipPSCP: pos_offset_prop size incorrect.");
	CheckConvParameter(speed_scale_prop.value(), njoints, 1.0, "DecoderManipPSCP: speed_scale_prop size incorrect.");
	CheckConvParameter(current_scale_prop.value(), njoints, 1.0, "DecoderManipPSCP: current_scale_prop size incorrect.");

	log(Info) << "DecoderManipPSCP plugin is reconfigured." << endlog();	
	return true;
}

void DecoderManipPSCP::decode(const char * buffer) 
{
	double tmp;
	for(int i = 0; i < njoints; i++) { 
		tmp = bswap_16(*(reinterpret_cast<const int16_t *>(buffer)));
		pos[i] = ScaleOffset( tmp, pos_scale_prop.get()[i], pos_offset_prop.get()[i] );
		tmp = bswap_16(*(reinterpret_cast<const int16_t *>(buffer+2)));
		speed[i] = Scale( tmp, speed_scale_prop.get()[i] );
		tmp = bswap_16(*(reinterpret_cast<const int16_t *>(buffer+4)));
		current[i] = Scale( tmp, current_scale_prop.get()[i] );
		pwm[i] = bswap_16(*(reinterpret_cast<const int16_t *>(buffer+6))) / 0x03ff; // convert 11-bit signed int to range (-1,1)
		buffer += 8;
	}
	speed_port.write(speed);
	current_port.write(current);
	pwm_port.write(pwm);
	pos_port.write(pos);
}

ORO_SERVICE_NAMED_PLUGIN( DecoderManipPSCP, "DecoderManipPSCP" )
