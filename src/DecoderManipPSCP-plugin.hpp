#ifndef  DECODERMANIPPSCP_PLUGIN_HPP
#define  DECODERMANIPPSCP_PLUGIN_HPP

#include <rtt/Port.hpp>
#include <rtt/Service.hpp>

#include "DecoderService.hpp"
#include "RTSerial-types.hpp"

/**
 * @ingroup Decoder
 * @brief Decode service plugin for array of the manipulator.
 * Decode input buffer as the array of big-endian MANIPPSCP-bit integers 
 * as position, speed, current and PWM.
 **/
class DecoderManipPSCP : public DecoderService
{
	RTT::OutputPort< std::vector<double> > pos_port; ///< Position.
	RTT::OutputPort< std::vector<double> > speed_port; ///< Speed.
	RTT::OutputPort< std::vector<double> > current_port; ///< Current.
	RTT::OutputPort< std::vector<double> > pwm_port; ///< PWM

	RTT::Property<int> njoints_prop;  ///< Number of joints.
	RTT::Property< std::vector<double> > pos_scale_prop; ///< Position scale factor.
	RTT::Property< std::vector<double> > pos_offset_prop; ///< Position offset.
	RTT::Property< std::vector<double> > speed_scale_prop; ///< Speed scale factor.
	RTT::Property< std::vector<double> > current_scale_prop; ///< Current scale factor.
	
	std::vector<double> pos, speed, current; ///< Temporary buffer for decoded data.
	std::vector<double> pwm; ///< Temporary buffer for decoded data.
	int njoints; ///< Current real array size. May be changed only by @ref configure call, when the conponent is sopped.
public:
	DecoderManipPSCP(RTT::TaskContext* c);
	~DecoderManipPSCP();
	
	bool configure();
	int getDataSize() const {
		return 2*4*njoints;
	}
	void decode(const char * buffer);
};

#endif  /*DECODERMANIPPSCP-PLUGIN_HPP*/
