extern "C" {
#include  <byteswap.h>
}

#include <rtt/RTT.hpp>
#include <rtt/plugin/Plugin.hpp>
#include <rtt/plugin/ServicePlugin.hpp>

#include <rtt/marsh/Marshalling.hpp>

#include "DataConversation.hpp"
#include "DecoderManipPSCS-plugin.hpp"

using namespace RTT;
using namespace std;
using namespace DataConversation;

DecoderManipPSCS::DecoderManipPSCS(RTT::TaskContext* context) :
	DecoderService("DecoderManipPSCS", context),
	njoints(0)
{
	this->doc("Manipulator input plugin.");
	// ports
	getOwner()->addPort("position", pos_port).
		doc("Position (10-bits).");
	getOwner()->addPort("speed", speed_port).
		doc("Speed.");
	getOwner()->addPort("current", current_port).
		doc("Current (10-bits).");
	getOwner()->addPort("state", state_port).
		doc("state");
	// properies
	getOwner()->provides()->addProperty("njoints", njoints_prop).
		doc("Number of manipulator joints.");
	// data conversation
	getOwner()->provides()->addProperty("pos_scale", pos_scale_prop).
		doc("Position scale factor.");
	getOwner()->provides()->addProperty("pos_offset", pos_offset_prop).
		doc("Position offset.");
	getOwner()->provides()->addProperty("speed_scale", speed_scale_prop).
		doc("Speed scale factor.");
	getOwner()->provides()->addProperty("current_scale", current_scale_prop).
		doc("Current scale factor.");
	// Attempt load property from config file.
	// getOwner()->getProvider<Marshalling>("marshalling")->readProperty( this->getName() + ".array_size", context->getName() + ".cpf");
	log(Info) << "DecoderManipPSCS plugin is loaded !" << endlog();	
}

DecoderManipPSCS::~DecoderManipPSCS()
{
	getOwner()->provides()->removeProperty(njoints_prop);
	getOwner()->provides()->removePort("position");
	getOwner()->provides()->removePort("speed");
	getOwner()->provides()->removePort("current");
	getOwner()->provides()->removePort("state");
}

bool DecoderManipPSCS::configure() 
{
	int size = njoints_prop.get();
	if (size <= 0 || size >= 10) {
		log(Error) << "DecoderManipPSCS plugin: incorrect 'njoints' property value: " << size << endlog(); 
		return false;
	}
	njoints = size;
	// set port size
	pos.resize(njoints);
	speed.resize(njoints);
	current.resize(njoints);
	state.resize(njoints);

	pos_port.setDataSample(pos);
	speed_port.setDataSample(speed);
	current_port.setDataSample(current);
	state_port.setDataSample(state);

	// Check conversation parameters
	CheckConvParameter(pos_scale_prop.value(), njoints, 1.0, "DecoderManipPSCS: pos_scale_prop size incorrect.");
	CheckConvParameter(pos_offset_prop.value(), njoints, 0.0, "DecoderManipPSCS: pos_offset_prop size incorrect.");
	CheckConvParameter(speed_scale_prop.value(), njoints, 1.0, "DecoderManipPSCS: speed_scale_prop size incorrect.");
	CheckConvParameter(current_scale_prop.value(), njoints, 1.0, "DecoderManipPSCS: current_scale_prop size incorrect.");

	log(Info) << "DecoderManipPSCS plugin is reconfigured." << endlog();	
	return true;
}

void DecoderManipPSCS::decode(const char * buffer) 
{
	double tmp;
	for(int i = 0; i < njoints; i++) { 
		tmp = bswap_16(*(reinterpret_cast<const int16_t *>(buffer)));
		pos[i] = ScaleOffset( tmp, pos_scale_prop.get()[i], pos_offset_prop.get()[i] );
		tmp = bswap_16(*(reinterpret_cast<const int16_t *>(buffer+2)));
		speed[i] = Scale( tmp, speed_scale_prop.get()[i] );
		tmp = bswap_16(*(reinterpret_cast<const int16_t *>(buffer+4)));
		current[i] = Scale( tmp, current_scale_prop.get()[i] );
		state[i] = bswap_16(*(reinterpret_cast<const int16_t *>(buffer+6)));
		buffer += 8;
	}
	speed_port.write(speed);
	current_port.write(current);
	state_port.write(state);
	pos_port.write(pos);
}

ORO_SERVICE_NAMED_PLUGIN( DecoderManipPSCS, "DecoderManipPSCS" )
