#ifndef  DECODERMANIPPSCS_PLUGIN_HPP
#define  DECODERMANIPPSCS_PLUGIN_HPP

#include <rtt/Port.hpp>
#include <rtt/Service.hpp>

#include "RTSerial-types.hpp"
#include "DecoderService.hpp"

/**
 * @ingroup Decoder
 * @brief Decode service plugin for array of the manipulator.
 * Decode input buffer as the array of big-endian MANIPPSCS-bit integers 
 * as position, speed, current and state.
 **/
class DecoderManipPSCS : public DecoderService
{
	RTT::OutputPort< std::vector<double> > pos_port; ///< Position.
	RTT::OutputPort< std::vector<double> > speed_port; ///< Speed.
	RTT::OutputPort< std::vector<double> > current_port; ///< Current.
	RTT::OutputPort< std::vector<int> > state_port; ///< Motor state.

	RTT::Property<int> njoints_prop;  ///< Number of joints.
	RTT::Property< std::vector<double> > pos_scale_prop; ///< Position scale factor.
	RTT::Property< std::vector<double> > pos_offset_prop; ///< Position offset.
	RTT::Property< std::vector<double> > speed_scale_prop; ///< Speed scale factor.
	RTT::Property< std::vector<double> > current_scale_prop; ///< Current scale factor.
	
	std::vector<double> pos, speed, current; ///< Temporary buffer for decoded data.
	std::vector<int> state; ///< Temporary buffer for decoded data.
	int njoints; ///< Current real array size. May be changed only by @ref configure call, when the conponent is sopped.
public:
	DecoderManipPSCS(RTT::TaskContext* c);
	~DecoderManipPSCS();
	
	bool configure();
	int getDataSize() const {
		return 2*4*njoints;
	}
	void decode(const char * buffer);
};

#endif  /*DECODERMANIPPSCS-PLUGIN_HPP*/
