#include "rtt/TaskContext.hpp"

#include "DecoderService.hpp"

DecoderService::DecoderService(const std::string& name, RTT::TaskContext * context) :
	RTT::Service(name, context) 
{
	// Add operations.
	getOwner()->addOperation("decoderConfigure", &DecoderService::configure, this).
		doc("Output port reconfiguration. Set new port width.");
	getOwner()->addOperation("decoderDecode", &DecoderService::decode, this).
		doc("Decode receive buffer content and write result to the port.");
	getOwner()->addOperation("decoderGetDataSize", &DecoderService::getDataSize, this).
		doc("Return data block length.");
}

DecoderService::~DecoderService() 
{
	getOwner()->provides()->removeOperation("decoderDecode");
	getOwner()->provides()->removeOperation("decoderGetDataSize");
	getOwner()->provides()->removeOperation("decoderConfigure");
}
