#ifndef  DECODERINTERFACE_HPP
#define  DECODERINTERFACE_HPP

#include <rtt/Service.hpp>

/**
 * @defgroup Decoder
 * @brief Decoder services.
 * @{
 */

/**
 * @brief Decoder service interface.
 **/
class DecoderInterface
{
	public:
		/**
		 * @brief Reconfigure decoder.
		 * Configure decoder and output ports accoding to properies.
		 * @return true on success.
		 **/
		virtual bool configure() = 0;

		/**
		 * @brief Get requied data buffer size.
		 **/
		virtual int getDataSize() const = 0; 

		/**
		 * @brief Decode buffer and write data to ports.
		 * Decode buffer and write data to corresponding ports.
		 * @param buffer pointer to the begin of data block.
		 */
		virtual void decode(const char * buffer) = 0;

		/**
		 * @brief Desructor.
		 * Desructor.
		 **/
		virtual ~DecoderInterface() {}
};

/**
 * @brief Decoder service base class.
 **/
class DecoderService : 
	public DecoderInterface, 
	public RTT::Service 
{
	public:
		DecoderService(const std::string& name, RTT::TaskContext * context);
		~DecoderService();
};

/**
 * @}
 */
#endif  /*DECODERINTERFACE_HPP*/
