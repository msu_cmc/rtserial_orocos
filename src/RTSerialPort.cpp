#include <cerrno>
#include <cstring>

extern "C" {
#include <fcntl.h>
#include <termios.h>
}

#include <rtt/Component.hpp>
#include <rtt/Logger.hpp>
#include <rtt/os/TimeService.hpp>
#include <rtt/marsh/Marshalling.hpp>

#include "RTSerialPort.hpp"

using namespace RTT;
using namespace RTT::os;

RTSerialPort::RTSerialPort(std::string const& name, TaskState initial_state) : 
	TaskContext(name, initial_state),
	port_fd(-1)
{
	// Register properies
	this->addProperty("baudrate", baudrate_prop).doc("Serial port baudrate.").set(57600);
	this->addProperty("port_name", port_name_prop).doc("Serial port name.");
	this->addProperty("parity", parity_prop).doc("Serial port parity mode: none, odd, even.").set("none");

	// Load properties from configuration file (if any presents)
	// this->getProvider<Marshalling>("marshalling")->updateProperties( this->getName() + ".cpf" );
}

RTSerialPort::~RTSerialPort()
{
	closeSerialPort();
}

bool RTSerialPort::closeSerialPort()
{
	// close serial port fd if necessary
	if (port_fd >= 0) {
		if (TEMP_FAILURE_RETRY(close(port_fd))) {
			log(Error) << "close() serial port failed: " << strerror(errno) << endlog(); 
		}
		port_fd = -1;
	}
}

bool RTSerialPort::configureSerialPort()
{
	struct termios tty;

	//TODO reopen only if port_name_prop changed 
	closeSerialPort();

	port_fd = open(this->port_name_prop.c_str(), O_RDWR | O_NOCTTY | O_SYNC );
	if (port_fd < 0) {
		log(Error) << "open() serial port \"" << this->port_name_prop << "\" failed: " << strerror(errno) << endlog(); 
		return false;
	}
	// configure serial port
	if (tcgetattr (this->port_fd, &tty) != 0) {
		log(Error) << "tcgetattr() failed: " << strerror(errno) << endlog(); 
		return false;
	}
	// 8-bits, 1 STOP bit, enable receiver, ignore modem lines
	tty.c_cflag |= CS8 | CREAD | CLOCAL; 
	tty.c_cflag &= ~CSTOPB;
	// set parity
	if (0 == this->parity_prop.compare("odd")) { tty.c_cflag |= PARENB | PARODD; }
	else if (0 == this->parity_prop.compare("even")) { tty.c_cflag |= PARENB; tty.c_cflag &= ~PARODD; }
	else if (0 == this->parity_prop.compare("none")) { tty.c_cflag &= ~(PARODD | PARENB); }
	else {
		log(Error) << "incorrect parity property value: " << this->parity_prop << endlog(); 
		return false;
	}
	// no signaling chars, no echo, no canonical processing
	tty.c_lflag = 0;
	// no special input processing
	tty.c_iflag = 0;
	// no special outptu processing
	tty.c_oflag = 0;
	// set speed
	switch (this->baudrate_prop) {
		case 9600:
			cfsetspeed (&tty, B9600);
			break;
		case 19200:
			cfsetspeed (&tty, B19200);
			break;
		case 38400:
			cfsetspeed (&tty, B38400);
			break;
		case 57600:
			cfsetspeed (&tty, B57600);
			break;
		case 115200:
			cfsetspeed (&tty, B115200);
			break;
		case 230400:
			cfsetspeed (&tty, B230400);
			break;
		default:
			log(Error) << "incorrect baudrate property value: " << baudrate_prop << endlog(); 
			return false;
	}
	//cfsetispeed(&tty, B0);
	//cfsetospeed(&tty, B0);
	// special properties
	tty.c_cc[VMIN]  = 0;            // read doesn't block
	tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout, so read will not block forever
	// configure port
	if (tcsetattr (this->port_fd, TCSANOW, &tty) != 0) {
		log(Error) << "tcsetattr() failed: " << strerror(errno) << endlog(); 
		return false;
	}
	return true;
}
