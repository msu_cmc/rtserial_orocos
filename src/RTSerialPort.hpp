#ifndef OROCOS_RTSERIALPORT_COMPONENT_HPP
#define OROCOS_RTSERIALPORT_COMPONENT_HPP

#include  <string>

#include <rtt/RTT.hpp>

/**
 * @brief Serial port component.
 * Contains common serial port properties and methods for opening and closing serial port.
 **/
class RTSerialPort : public RTT::TaskContext
{
protected:
	int port_fd;
	
	// serial port properties
	std::string port_name_prop;
	int baudrate_prop;
	std::string parity_prop;

protected:
	/** 
	 * @brief RTSerialPort constructor.
	 * Hand over arguments to @c RTT::TaskContext constructor.
	 */
	RTSerialPort(std::string const& name, RTT::base::TaskCore::TaskState initial_state = RTT::base::TaskCore::Stopped);

	/**
	 * @brief Destroy object and close serial port.
	 */
	~RTSerialPort();

	/**
	 * @brief Close port.
	 * Close opened serial port or do nothing.
	 * @return true on success, false otherwise
	 **/
	bool closeSerialPort();

	/**
	 * @brief Open (if necessary) and configure serial port.
	 * Open (if necessary) serial port and configure it according to properties.
	 * @return true on success, false on failure.
	 **/
	bool configureSerialPort();
};

#endif
