#include "RTSerialRecv-component.hpp"

#include <rtt/Component.hpp>
#include <rtt/Logger.hpp>
#include <rtt/os/TimeService.hpp>
#include <rtt/marsh/Marshalling.hpp>

#include <cerrno>
#include <cstring>
#include <iostream>

extern "C" {
#include <fcntl.h>
#include <termios.h>
}

using namespace std;
using namespace RTSerial;
using namespace RTT;
using namespace RTT::os;

#define FB_BYTE 'A'
#define TAIL_BYTE 'B'

RTSerialRecv::RTSerialRecv(std::string const& name) : 
	RTSerialPort(name),
	state(STOPPED),
	frame_buffer(0)
{
	//get an instance	of TimeService.
	time_service = TimeService::Instance();
	// Register ports
	this->addPort("stat", stat_port).doc("Statistics output port.");
	this->addPort("notify", fb_notify_port).doc("Output port that raises frame start event.");

	// Load properties from configuration file (if any presents)
	//this->getProvider<Marshalling>("marshalling")->updateProperties( this->getName() + ".cpf" );

	log(Info) << "RTSerialRecv constructed !" << endlog();	
}

bool RTSerialRecv::configureHook()
{
	log(Info) << "RTSerialRecv configuration started !" << endlog();	
	// Reconfigure serial port
	if (! configureSerialPort()) return false;	
	// Probe decoder plugin.
	OperationCaller<bool()> decoder_configure_call(this->getOperation("decoderConfigure"));
	// If decoder plugin presents reconfigure it.
	if (decoder_configure_call.ready()) {
		if (! decoder_configure_call()) return false;
	}
	log(Info) << "RTSerialRecv configuration finished !" << endlog();	
	return true;
}

bool RTSerialRecv::startHook(){
	// Reconfigure serial port
	if (! configureSerialPort()) return false;	
	
	// Probe decoder plugin.
	OperationCaller<bool()> decoder_configure_call(this->getOperation("decoderConfigure"));
	OperationCaller<int()> decoder_getdatasize_call(this->getOperation("decoderGetDataSize"));
	if (!decoder_call.ready()) {
		decoder_call = this->getOperation("decoderDecode");
	}
	if (!decoder_call.ready() || !decoder_configure_call.ready() || !decoder_getdatasize_call.ready()) {
		log(Error) << "RTSerialRecv decoder plugin is not ready." << endlog(); 
		return false;
	}
	//reconfigure decoder
	decoder_configure_call();
	//Setup frame_buffer and frame size;
	frame_size = 3 + decoder_getdatasize_call();
	frame_buffer = new char[frame_size + 1];
	frame_buffer[frame_size] = 0;
	//reset statistcs
	memset(&statistics, 0, sizeof(statistics));
	statistics.frame_state = RECV_FRAME_NO_ACTIVITY;
	memset(&notify_msg, 0, sizeof(notify_msg));
	//set state
	state = SYNCING;
	tcflush(port_fd, TCIFLUSH);
	log(Info) << "RTSerialRecv configuration finished !" << endlog();	
	return true;
}

void RTSerialRecv::updateHook() 
{
	int retval;
	nsecs frame_end_time;

	//cout << "S=" << state << "P=" << port_fd << endl;

	switch (state) {
		case RECEIVING_FB:
			//receive fb
			retval = read(port_fd, frame_buffer, 1);
			notify_msg.frame_recv_time = time_service->getNSecs();
			if (retval == 0) break;
			else if (retval < 0) {
				if (errno == EINTR) break;
				else goto error;
			}
			if (frame_buffer[0] != FB_BYTE) {
				//nothing received or incorrect frame 
				//try to sync again
				state = SYNCING;
				break;
			}
			state = RECEIVING_SEQ;

		case RECEIVING_SEQ:
			//receive sequence number
			retval = read(port_fd, frame_buffer + 1, 1);
			if (retval == 0) break;
			else if (retval < 0) {
				if (errno == EINTR) break;
				else goto error;
			}
			notify_msg.seq_num1 = frame_buffer[1];
			//notify other components
			fb_notify_port.write(notify_msg);	
			//prepare to receive the last part of frame
			frame_nbyte_read = 2; 
			state = RECEIVING_DATA_TAIL;

		case RECEIVING_DATA_TAIL:
			retval = read(port_fd, frame_buffer + frame_nbyte_read, frame_size - frame_nbyte_read);
			if (retval == -1) {
				if (errno == EINTR) break;
				else goto error;
			}
			frame_nbyte_read += retval;
			if (frame_nbyte_read < frame_size) break; // try again later
			//full frame received
			if (frame_buffer[frame_size - 1] != TAIL_BYTE) {
				//incorrect frame
				statistics.error_cnt++;
				statistics.frame_state = RECV_FRAME_ERR;
				//publish statistics
				stat_port.write(statistics);
				
				state = SYNCING;
				break;
			}
			//correct frame received
			frame_end_time = time_service->getNSecs();
			//decode frame
			decoder_call(frame_buffer + 2);
			//update statistics
			statistics.frame_cnt++;
			//calculate period and length only if previous frame is correct
			if (statistics.frame_state == RECV_FRAME_OK) {
				statistics.frame_duration = frame_end_time - notify_msg.frame_recv_time;
				statistics.frame_period = notify_msg.frame_recv_time - statistics.frame_recv_time;
			}
			statistics.frame_recv_time = notify_msg.frame_recv_time;
			statistics.frame_state = RECV_FRAME_OK;
			//publish statistics
			stat_port.write(statistics);
			state = RECEIVING_FB;
			break;

		case SYNCING:
			//receive full frame
			frame_nbyte_read = 0;
			state = SYNCING_TAIL;

		case SYNCING_TAIL:
			retval = read(port_fd, frame_buffer + frame_nbyte_read, frame_size - frame_nbyte_read);
			if (retval == -1) {
				if (errno == EINTR) break;
				else goto error;
			}
			frame_nbyte_read += retval;
			if (frame_nbyte_read < frame_size) break; // try again later
			//frame buffer is full, check its correctness.
			if (frame_buffer[0] == FB_BYTE && frame_buffer[frame_size - 1] == TAIL_BYTE) {
				//correct frame received, switch to normal operation mode
				state = RECEIVING_FB;
				break;
			}
			{
				//cout << "sframe:";
				//for(int i = 0; i < frame_size; i++) { cout << " " << static_cast<unsigned int>(frame_buffer[i]); }
				//cout << endl;

				char * fb_ptr = (char *) memchr(frame_buffer + 1, FB_BYTE, frame_size - 1);
				if (fb_ptr == 0) {
					//nothing found
					state = SYNCING;
					break;
				}
				frame_nbyte_read = frame_size - (fb_ptr - frame_buffer);
				memmove(frame_buffer, fb_ptr, frame_nbyte_read);
			}
			break;

		case STOPPED:
			tcflush(port_fd, TCIFLUSH);
			break;
	}
	this->trigger();
	return;

error:
	log(Error) << "RTSerialRecv read port failed:" << strerror(errno) << endlog(); 
	this->exception();
}

void RTSerialRecv::stopHook() 
{
	//clear buffers
	delete [] frame_buffer;
	state = STOPPED;
	log(Info) << "RTSerialRecv execution finished !" << endlog();	
}

void RTSerialRecv::cleanupHook()
{
	closeSerialPort();
	log(Info) << "RTSerialRecv cleaning up !" << endlog();	
}

ORO_CREATE_COMPONENT(RTSerialRecv)
