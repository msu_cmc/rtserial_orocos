#ifndef OROCOS_RTSERIALRECV_COMPONENT_HPP
#define OROCOS_RTSERIALRECV_COMPONENT_HPP

#include  <string>

#include <rtt/RTT.hpp>
#include <rtt/Port.hpp>
#include <rtt/Operation.hpp>

#include    "RTSerial-types.hpp"
#include    "RTSerialPort.hpp"

/**
 * @addtogroup Components
 * @brief OROCOS components.
 * @{
 */

/**
 * @brief Serial protocol frame receiver component class.
 * 
 * This OROCOS component is used to recive data frames from target device. Component interface is self explanatory. 
 *
 * Each time new correct frame is received it contents is written to output port and receive statistics port is 
 * updated (@ref RTSerial::recv_stat). Notify message (@ref RTSerial::recv_notify) on @a notify port is formed after startbyte and sequence number of next 
 * frame are received. It contains timestamp of startbyte and sequence number.
 *
 * Component unable to function properly without @ref Decoder plugin.
 *
 * @see{RTSerialSend, RTSerial::recv_stat, RTSerial::recv_notify}
 *
 **/
class RTSerialRecv : public RTSerialPort
{
private:
	enum {
		RECEIVING_FB,
		RECEIVING_SEQ,
		RECEIVING_DATA_TAIL,
		SYNCING,
		SYNCING_TAIL,
		STOPPED,
	} state; 

	//frame
	char * frame_buffer;
	int frame_size;
	int frame_nbyte_read;
	
	// updateHook variables
	struct RTSerial::recv_notify notify_msg;
	struct RTSerial::recv_stat statistics;
	RTT::os::TimeService * time_service;

	// serial port properties
	std::string port_name_prop;
	int baudrate_prop;
	std::string parity_prop;

	// frame decoder
	//std::string decoder_name_prop; 
	RTT::OperationCaller<void (const char *)> decoder_call;

	// input/output ports
	RTT::OutputPort<RTSerial::recv_stat> stat_port;
	RTT::OutputPort<RTSerial::recv_notify> fb_notify_port;

public:
	RTSerialRecv(std::string const& name);
	bool configureHook();
	bool startHook();
	void updateHook();
	void stopHook();
	void cleanupHook();
};

/**
 * @}
 */

#endif
