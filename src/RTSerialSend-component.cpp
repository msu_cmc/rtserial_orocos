#include "RTSerialSend-component.hpp"

#include <rtt/Component.hpp>
#include <rtt/Logger.hpp>
#include <rtt/os/TimeService.hpp>
#include <rtt/marsh/Marshalling.hpp>

#include <cerrno>
#include <cstring>
#include <iostream>

extern "C" {
#include <fcntl.h>
#include <termios.h>
}

using namespace std;
using namespace RTSerial;
using namespace RTT;
using namespace RTT::os;

#define FB_BYTE 'A'
#define TAIL_BYTE 'B'

RTSerialSend::RTSerialSend(std::string const& name) : 
	RTSerialPort(name),	
	frame_buffer(0),
	sync_mode(false),
	wait_lock(0)
{
	this->getActivity()->thread()->setScheduler(ORO_SCHED_RT);
	this->getActivity()->thread()->setPriority(HighestPriority);
	//get an instance	of TimeService.
	time_service = TimeService::Instance();
	// Register ports
	this->addPort("stat", stat_port).
		doc("Statistics output port."),
	this->addEventPort("notify", notify_port).
		doc("Input port for syncronization with external events.");

	// Register properies
	this->addProperty("sync_mode", sync_mode_prop).
		doc("Syncronous mode. The next frame is send with fixed delay after notify_port event. Otherwise new data are send as soon as it is available.").
		set(false);
	this->addProperty("delay", delay_prop).
		doc("Time interval between notity event and beginig of outgoing frame in nanoseconds for syncronious mode.").
		set(0);

	// Register operatons
	this->addOperation( "setDelay", &RTSerialSend::setDelay, this, OwnThread).
		doc("Change 'delay' for running component").
		arg("delay", "dealy in nanoseconds");

	// Load properties from configuration file (if any presents)
	// this->getProvider<Marshalling>("marshalling")->updateProperties( this->getName() + ".cpf" );

	log(Info) << "RTSerialSend constructed !" << endlog();	
}

void RTSerialSend::setDelay(double _delay)
{
	//Called in OwnThread so is threadsafe.
	delay_prop = _delay;
	delay = _delay;
}

bool RTSerialSend::configureHook()
{
	log(Info) << "RTSerialSend configuration started !" << endlog();	
	// Reconfigure serial port
	if (! configureSerialPort()) return false;	
	// Probe coder plugin.
	OperationCaller<bool()> coder_configure_call(this->getOperation("coderConfigure"));
	// If coder plugin presents reconfigure it.
	if (coder_configure_call.ready()) {
		if (! coder_configure_call()) return false;
	}
	log(Info) << "RTSerialSend configuration finished !" << endlog();	
	return true;
}

bool RTSerialSend::startHook()
{
	// Reconfigure serial port
	if (! configureSerialPort()) return false;	
	// Probe coder plugin.
	OperationCaller<bool()> coder_configure_call(this->getOperation("coderConfigure"));
	OperationCaller<int()> coder_getdatasize_call(this->getOperation("coderGetDataSize"));
	if (!coder_call.ready()) {
		coder_call = this->getOperation("coderEncode");
	}
	if (!coder_call.ready() || !coder_configure_call.ready() || !coder_getdatasize_call.ready()) {
		log(Error) << "RTSerialSend coder plugin is not ready." << endlog(); 
		return false;
	}
	//reconfigure coder
	coder_configure_call();
	//Setup frame_buffer and frame size;
	frame_size = 3 + coder_getdatasize_call();
	frame_buffer = new char[frame_size];
	frame_buffer[0] = FB_BYTE;
	frame_buffer[frame_size - 1] = TAIL_BYTE;
	//reset statistcs
	memset(&statistics, 0, sizeof(statistics));
	memset(&notify_msg, 0, sizeof(notify_msg));
	//set operation mode
	sync_mode = sync_mode_prop;
	frame_nbyte_written = frame_size;
	//set delay
	delay = delay_prop;
	log(Info) << "RTSerialSend configuration finished !" << endlog();	
	return true;
}

void RTSerialSend::updateHook() 
{
	int retval;
	nsecs timestamp;

	//Event was triggered, so check its type.
	if (sync_mode) {
		if ( RTT::NewData != notify_port.read(notify_msg, false) ) {
			//Wakeup has not been caused by notify event.
			//Contine waiting for event.
			return;
		}
		//Setup delay. Event processing is stopped.
		//TODO signal semaphore 'wait_lock' in breakUpdateHook()
		timestamp = notify_msg.frame_recv_time + delay;
		//if (time_service->getNSecs() > timestamp) return;
		wait_lock.waitUntil(timestamp);
		//Extract data
		coder_call(frame_buffer + 2);
	}
	else {
		if (! coder_call(frame_buffer + 2)) {
			//No data on input port. Wait for next event.
			return;
		}
		notify_port.read(notify_msg, false);
	}

	//begin new frame
	statistics.frame_cnt++;
	//set new seq number
	frame_buffer[1] = statistics.frame_cnt & 0xff;
	//send frame
	timestamp = time_service->getNSecs();
	frame_nbyte_written = 0;
	do {
		retval = TEMP_FAILURE_RETRY(write(port_fd, frame_buffer + frame_nbyte_written, frame_size - frame_nbyte_written));
		if (retval == -1) goto error;
		frame_nbyte_written += retval;
	}
	while (frame_nbyte_written < frame_size);
	//update and publish statisticks
	statistics.frame_send_time = timestamp;
	statistics.frame_duration = time_service->getNSecs(timestamp);
	statistics.seq_num_delay = frame_buffer[1] - notify_msg.seq_num1;
	statistics.send_delay = timestamp - notify_msg.frame_recv_time;
	stat_port.write(statistics);
	//
	/*for(int i = 0; i < frame_size; i++) {
		std::cout << frame_buffer[i];
	}
	std::cout << std::endl;*/
	return;

error:
	log(Error) << "RTSerialSend write port failed:" << strerror(errno) << endlog(); 
	this->exception();
}

void RTSerialSend::stopHook() {
	//clear buffers
	delete [] frame_buffer;
	log(Info) << "RTSerialSend execution finished !" << endlog();	
}

void RTSerialSend::cleanupHook() {
	closeSerialPort();
	log(Info) << "RTSerialSend cleaning up !" << endlog();	
}

ORO_CREATE_COMPONENT(RTSerialSend)

