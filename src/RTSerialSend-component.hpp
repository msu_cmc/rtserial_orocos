#ifndef OROCOS_RTSERIALSEND_COMPONENT_HPP
#define OROCOS_RTSERIALSEND_COMPONENT_HPP

#include  <string>

#include <rtt/RTT.hpp>
#include <rtt/Port.hpp>
#include <rtt/Operation.hpp>
#include <rtt/os/Semaphore.hpp>
#include <rtt/os/Time.hpp>
#include <rtt/os/TimeService.hpp>

#include "RTSerial-types.hpp"

#include "RTSerialPort.hpp"

/**
 * @addtogroup Components
 * @brief OROCOS components.
 * @{
 */

/**
 * @brief Serial protocol frame sender component class.
 * 
 * This OROCOS component is used to send data frames from target device. Component interface is self explanatory. 
 *
 * In syncronious mode frame is sent to targer device immediatly after new data is written into input port.
 * In asyncronous mode after receiving notification message on @a notify port component extracts timestamp from it, adds to timestamp 
 * specified delay (nanoseconds), waits until result time moment and sends next frame. Current value of input 
 * port is used.
 *
 * After each sent frame statistics port (@ref RTSerial::send_stat) is updated.
 *
 * Component is unable to function properly without @ref Coder plugin.
 *
 * @see{RTSerialRecv, RTSerial::send_stat, RTSerial::recv_notify}
 *
 **/
class RTSerialSend : public RTSerialPort
{
private:
	//operation mode
	bool sync_mode;
	long delay;

	//properties
	bool sync_mode_prop;
	double delay_prop;
	
	//buffer
	char * frame_buffer;
	int frame_size;
	int frame_nbyte_written;
	
	// updateHook variables
	struct RTSerial::recv_notify notify_msg;
	struct RTSerial::send_stat statistics;
	RTT::os::Semaphore wait_lock;
	RTT::os::TimeService * time_service;

	// frame coder
	//std::string coder_name_prop; 
	RTT::OperationCaller<bool (char *)> coder_call;

	// input/output ports
	RTT::OutputPort<RTSerial::send_stat> stat_port;
	RTT::InputPort<RTSerial::recv_notify> notify_port;

public:
	RTSerialSend(std::string const& name);

	void setDelay(double _delay);

protected:
	bool configureHook();
	bool startHook();
	void updateHook();
	void stopHook();
	void cleanupHook();
};

/**
 * @}
 */

#endif
