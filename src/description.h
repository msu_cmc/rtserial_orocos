/**
@file description.h 
@brief File with Doxygen documentation.

@mainpage RTSerial Documentation

Overview
--------

RTSerial is OROCOS component library to data exchange with target device over serial port (RS232) in real time.
Targed device is usualy a microcontroller which is used to read data from sensors and directly control hardware.
Any embedded development board equipped with serial communication port can be used for such purpose.


Library contains following parts:

- @ref RTSerialRecv component receives data frame from target device.
- @ref RTSerialSend component sends data frame to target device. Support syncronious and asycronous mode.
- @ref Decoder plugins are used translate received frame content to recive component output port data type.
- @ref Coder plugins are used translate input port data type to target device frames.
- Typekit for data types which represent statistics and notification messages.

In asyncronous mode @ref RTSerialRecv sends next frame immediatly after new data arrives on component input port.
In syncronious mode component sends current data on input port with specified delay after a notification message (new data on @a notify port).
More precisly after receiving notification event component waits until (timestamp in notification message + specified delay) and then sends next frame.
@ref RTSerialRecv component can produce those notification messages after reveiving start byte, so next frame is always sent to target device with fixed delay after 
begining of input frame.

Serial Communication Protocol
-----------------------------

The frame of Communication Protocol has a very simple structure:

<table>
<tr> 
<td> @c STARTBYTE </td> 
<td> @c SEQ </td>  
<td> @c DATA_BYTE1 ... @c DATA_BYTEn </td> 
<td> @c TAILBYTE </td>
</tr>
</table>

@c STARTBYTE and @c TAILBYTE are to fixed bytes which are used to detect frame in the stream.
See defines in source files for actual values.

@c SEQ is byte containing frame sequence number. @ref RTSerialSend component write to this byte 
least significat part of frame number. When target device processes received frame it copies sequence number 
from to next sent frame. So OROCOS software can detect if last send frame was processed using by 
comparing sequence number of last received and last sent frame.

@c DATA_BYTEs contains transmitted data. The length of data section and its interpretation depends on @ref 
@ref Decoder and @ref Coder plugins. Usually this section contains array of @c int16 or @c int32 numbers.

Serial link set-up is 1 start bit, 8 data bits, 1 stop bit. Parity mode and baudrate can be configured.


Usage examples
--------------

Setup @c receiver to decode input frame as array of three little-endian int16.
Configured component has three output ports: @a out (data, @c vector<double>), @a stat (statistics, @ref RTSerial::recv_stat),
@a notify (start byte receive notification message, @ref RTSerial::recv_notify).

@code
	import("RTSerial")

	loadComponent("receiver", "RTSerialRecv")
	receiver.port_name = "/dev/ttyS0"
	receiver.baudrate = 115200
	receiver.parity = "even"
	loadService("receiver", "DecoderInt16")
	receiver.byteswap = false
	receiver.array_size = 3
@endcode

Setup @c sender in ayncronous mode. Int16 are big-endian.
Configured component has one output ports @a stat (statistics, @ref RTSerial::send_stat) 
and two input ports: @a in (data, @c vector<double>), @a notify (notification message).

@code
	loadComponent("sender", "RTSerialSend")
	sender.port_name = "/dev/ttyS0"
	sender.baudrate = 115200
	sender.parity = "even"
	sender.sync_mode = false
	loadService("sender", "CoderInt16")
	receiver.byteswap = true
	receiver.array_size = 3
@endcode

Setup @c sender in syncronous mode. Delay is equal to 6 ms.

@code
	loadComponent("sender", "RTSerialSend")
	sender.port_name = "/dev/ttyS0"
	sender.baudrate = 115200
	sender.parity = "even"
	sender.sync_mode = true
	sender.delay = 6000000

	ConnPolicy cp
	connect("receiver.notify", "sender.notify", cp)
@endcode

Futher improvments
------------------

1. Type conversation: scale, offset and continous signal "unwrap" for Int16.
2. Select proper datatype for timestamps.
3. 32-bit Caoder and Decoder versions.

**/
